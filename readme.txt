AGRESERVATIONS README

The goal of the modules in this package
is: To provide you with the necesary tools to run a
hotelwebsite 

1.: make sure the following modules are installed: calendar, date_popup, resource_conflict(version >= 6.x-2.x-dev !!) , date_timezone, content, content_copy, number, userreference and for commercial use the ubercart core modules.

2.: install the agreservations modules.


3.: under http://yoursitesurl/admin/content/node-type/agreservation
  enable the agreservation contenttype for Resouce Conflict
  checking by choosing the Resource Conflict datetime field and the booked
 resource reference field to check for conflicts, obviously these are:
 Reservationdate and unitreference.

4.: add at least one (or more) agreservations_unittype like: standard,
 large,...if ubercart is enabled: in the ubercartshopconfiguration at
  http://yoursitesurl/admin/store/products/classes make this contenttype
 a productclass. Mark the agreservation_unittypes as non shippable.

5.: add some agreservations units like: Room #1, Guestroom, Room#2, the boat,...

6.: the module contains a default view bookingcalendar which should be
 installed automatically. When you followed the above steps you should
 see a matrix of resources and units. You can click on the little '+' signs
 for each day/room and make a booking, you see the agreservation form prefilled
 with the correct day and room you clicked on. You can choose more rooms of course.

7.: the agres_onlineform module brings a searchwidget Block which you can activate
 in Administer->blocks. This would be for the frontpage for users to make onlinereservations.

8.: If you have ubercart installed and agres_bookings activated you will see
 a new order_pane in the Admin create order form. Here you can quickly check
 if rooms are bookable in the desired time and make the reservation.
 This is usefull i.e. if a guest calls you by phone to make a reservation.

9.: For Ubercart you can use your own templates for invoices.
 Agreservations provides an adapted standard template with the name customer_agres.itpl.
 You can activate it by going to Store administration/Configuration/Order settings/Customer settings and choose it.
 You can also make a copy of it and make your own changes and then choose your version.
 The correct directory for the templates is: ubercart/uc_order/templates where ubercart is the name of the module folder in sites/all/modules.