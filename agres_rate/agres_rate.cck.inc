<?php
/**
 * @file
 * Implements the agres_rate content type.
 */
function agres_rate_get_agres_rate_content_type(){
$content['type']  = array (
  'name' => 'Agreservations Price Rate',
  'type' => 'agres_rate',
  'description' => 'Rates contain prices or price modifiers which affect or verride the prices set in the Unit type / products of agreservations and ubercart. Rates can additionally be tied to timeframes giving you the possibility to set up seasonaly different prices.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'language_content_type' => '0',
  'upload' => '1',
  'old_type' => 'agres_rate',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'comment' => 2,
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content['fields']  = array (
  0 =>
  array (
    'label' => 'Reference Timeframe',
    'field_name' => 'field_agres_ref_timeframe',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => '8',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' =>
    array (
      'agres_timeframe' => 'agres_timeframe',
      'agres_rate' => 0,
      'page' => 0,
      'product' => 0,
      'agreservation' => 0,
      'story' => 0,
      'agreservations_unit' => 0,
      'agreservation_unit_info' => 0,
      'agreservations_unittype' => 0,
      'agres_category' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'cck_referential_integrity_behavior' => 'set_null',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array (
      'nid' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' =>
    array (
      'label' =>
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '-2',
  'revision_information' => '2',
  'author' => '1',
  'options' => '3',
  'comment_settings' => '4',
  'menu' => '-3',
  'taxonomy' => '-4',
  'path' => '5',
  'attachments' => '6',
  'base' => '-1',
  'body' => '0',
);

  return $content;
}

function agres_rate_get_unittype_content_type_patch(){
 $content['type']  = array (
  'name' => 'Unit type',
  'type' => 'agreservations_unittype',
  'description' => 'The Type of unit by agreservations module:
Single unit, Double unit, Suite, Presidents Suite, Guestunit...',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'language_content_type' => '0',
  'upload' => 1,
  'old_type' => 'agreservations_unittype',
  'orig_type' => '',
  'module' => 'uc_product',
  'custom' => '0',
  'modified' => '1',
  'locked' => '0',
  'reset' => 'Reset to defaults',
  'uc_product_shippable' => 1,
  'uc_image' => '',
  'comment' => 2,
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content['fields']  = array (
  0 =>
  array (
    'label' => 'Reference Rate',
    'field_name' => 'field_agres_ref_rate',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'change' => 'Change basic information',
    'weight' => '32',
    'autocomplete_match' => 'contains',
    'size' => 60,
    'description' => '',
    'default_value' =>
    array (
      0 =>
      array (
        'nid' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 0,
    'multiple' => '1',
    'referenceable_types' =>
    array (
      'agres_rate' => 'agres_rate',
      'page' => 0,
      'panel' => 0,
      'product' => 0,
      'agreservation' => 0,
      'story' => 0,
      'agres_timeframe' => 0,
      'agreservations_unit' => 0,
      'agreservation_unit_info' => 0,
      'agreservations_unittype' => 0,
      'agres_category' => 0,
      'testproduct' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'cck_referential_integrity_behavior' => '',
    'op' => 'Save field settings',
    'module' => 'nodereference',
    'widget_module' => 'nodereference',
    'columns' =>
    array (
      'nid' =>
      array (
        'type' => 'int',
        'unsigned' => true,
        'not null' => false,
        'index' => true,
      ),
    ),
    'display_settings' =>
    array (
      'weight' => '32',
      'parent' => '',
      'label' =>
      array (
        'format' => 'above',
      ),
      'teaser' =>
      array (
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' =>
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '0',
  'revision_information' => '20',
  'author' => '20',
  'options' => '25',
  'language' => '0',
  'translation' => '30',
  'menu' => '-2',
  'taxonomy' => '-3',
  'path' => '30',
  'base' => '-1',
  'body' => '1',
);

  return $content;
}