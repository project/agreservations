<?php
/**
 * @file
 * helper functions. not only usefull for agreservations
 */

/**
  * in_multi_assoc_array: returns BOOLEAN.
  *
  * @author     Andreas Gill
  * @param      $needle zu suchender Wert.
  * @param      $array   zu durchsuchendes Array.
  * @return     TRUE oder FALSE
  */
    function in_multi_assoc_array($needle, $array)
    {
        foreach ($array as $element){
           if ($element == $needle) {
               return true;
           }else{
               if (is_array($element)){
                 if(in_multi_assoc_array($needle,$element)){
                     return true;
                 }
               }
           }
        }
        return false;
    }
    /**
  * _is_time_in_range: returns BOOLEAN.
  *
  * @author     Andreas Gill
  * @param      $timetocheck time to check.timestring'H:i'
  * @param      $rangelow lower end of timerange.timestring'H:i'
  * @param      hrangehigh higher end of timerange.timestring of form 'H:i'
  * @return     TRUE if in range or FALSE if not within range
  */
    function _is_time_in_range($timetocheck, $rangelow,$rangehigh){
      $strtimetocheck = date('H:i',strtotime($timetocheck));
       if ((strtotime($strtimetocheck) >= strtotime($rangelow))&&(strtotime($strtimetocheck) <= strtotime($rangehigh))) {        
          return TRUE;
       }
       else {
         return FALSE;
       }
    }
  /**
  * _is_date_in_range: returns BOOLEAN.
  *
  * @author     Andreas Gill
  * @param      $datetocheck, date to check. timestring 'Y-m-d'
  * @param      $rangelow lower end of daterange.timestring'Y-m-d'
  * @param      hrangehigh higher end of daterange.timestring of form 'Y-m-d'
  * @return     TRUE if in range or FALSE if not within range
  */
    function _is_date_in_range($datetocheck, $rangelow,$rangehigh){
                      
      $strdatetocheck = date_format(date_make_date($datetocheck), 'Y-m-d');
      $strrangelow = date_format(date_make_date($rangelow), 'Y-m-d');
      $strrangehigh = date_format(date_make_date($rangehigh), 'Y-m-d');
       if ((strtotime($strdatetocheck) >= strtotime($strrangelow))&&(strtotime($strdatetocheck) <= strtotime($strrangehigh))) {
//         drupal_set_message('<pre>_is_date_in_range'.strtotime($rangelow).'--'.$strrangelow.'--'.$strrangehigh.' </pre>');
          return TRUE;
       }
       else {
//         drupal_set_message('<pre>_is_date_NOT_in_range'.$strdatetocheck.'--'.$strrangelow.'--'.$strrangehigh.' </pre>');
         return FALSE;
       }
    }

 /**
  * get_current_path: current url
  * from drupaluser dhaneshharidas 
  * from http://drupal.org/node/46088
  */
  function get_current_path() {
  $current_path = explode('=', drupal_get_destination());
  // Extracting URL from $current_path
  if (is_array($current_path) && count($current_path) >= 2) {
    if (trim($current_path[1]) != '') {
      $current_url_full = htmlspecialchars(urldecode($current_path[1]));
      // Removing query string
      $current_url_elements = explode('?', $current_url_full);
      if (is_array($current_url_elements)) {
        return trim($current_url_elements[0]);
      } else {
        return trim($current_url_elements);
      }
    } else {
      return $_REQUEST['q'];
    }
  }
}

 /**
  * createDateRangeArray: current url
  * @author   Mike Boone
  * @param    $strDateFrom Y-m-d
  * @param    $strDateTo Y-m-d
  * @return   creates an inclusive array of the dates between the from and to dates.
  * from http://boonedocks.net/mike/archives/137-Creating-a-Date-Range-Array-with-PHP.html
  */
function createDateRangeArray($strDateFrom,$strDateTo) {
  // takes two dates formatted as Y-m-d and creates an
  // inclusive array of the dates between the from and to dates.

  // could test validity of dates here but I'm already doing
  // that in the main script
  $aryRange=array();

  $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
  $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

  if ($iDateTo>=$iDateFrom) {
    array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry

    while ($iDateFrom<$iDateTo) {      
      $iDateFrom=strtotime("+1 day",$iDateFrom);//86400; // add 24 hours
      array_push($aryRange,date('Y-m-d',$iDateFrom));
    }
  }
  return $aryRange;
}

 /**
this is a helperfunctions that calls and aplies the filter
  * from the language sections module. makes only sense when ls is
  * installed.
  */

   function _agres_call_ls_filter($format = -1, $text = '') {
     if (!module_exists('language_sections')) {
       return $text;
     }
     extract(_language_sections_get_ids($format));
     // Quick-exit if disabled (e.g. by LS Search).
     if (isset($GLOBALS['language_sections_disable'])) {
       return $text;
     }
     // Get $pattern and $triggers.
     $pattern = _language_sections_setting($mod_prefix, 'pattern');
     $triggers = _language_sections_get_triggers($mod_prefix);

     // Get values for $current_language, $all_languages, $other_languages
     extract(_language_sections_context('match_types'));
     $n1 = $n2 = 2;
     $n3 = 4;  // indexes to use with array from preg_split().
//     drupal_set_message('<pre>***ls'.print_r($text, true).'</pre>');
     $matches = preg_split($pattern, $text, -1, PREG_SPLIT_DELIM_CAPTURE);
     //drupal_set_message(print_r($matches, 1), 'warning');
     // Build the output string, keeping only the parts we want...
     $out = $matches[0];
     $show_default = TRUE;
     for ($i = $n1; $i < count($matches); $i += $n3) {
       // Convert to lower case.
       $trigger = strtolower($matches[$i]);
       // No matching language trigger so continue with next section.
       if (!isset($triggers[$trigger]))
         continue;

       switch ($triggers[$trigger]) {
         // case: a section for the current language, use it and clear "use default" flag.
         case $current_language:
           $out .= $matches[$i + $n2];
           $show_default = FALSE;
           break;
         // case: a section for "all languages", use it.
         case $all_languages:
           $out .= $matches[$i + $n2];
           break;
         // case: a "default" section, use it if we haven't already used a language-specific section...
         case $other_languages:
           if ($show_default) {
             $out .= $matches[$i + $n2];
           }
           else {
             $show_default = TRUE;
           }
           break;
       }
     }
     return $out;
   }